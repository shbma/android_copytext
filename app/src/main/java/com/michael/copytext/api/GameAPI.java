package com.michael.copytext.api;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by michael on 27.05.16.
 */
public interface GameAPI {
    @FormUrlEncoded
    @POST("index2.php")
    Call<State> getState(
            @Field("jax") String jax,
            @Field("to") int to
    );
}

/*
* {
*  "node":
*   {"id":"1",
*   "label":"\u0412\u043e\u0442 \u043d\u0435\u0437\u0430\u0434\u0430\u0447\u0430... \u041d\u0435 \u0437\u043d\u0430\u044e, \u0433\u0434\u0435 \u043e\u0442\u043e\u0431\u0435\u0434\u0430\u0442\u044c. \u041c\u0430\u043a\u0441\u0438\u043c\u0438\u043b\u043b\u0438\u0430\u043d\u0441 \u0438\u043b\u0438 \u041a\u0438\u0442\u0430\u0439\u0449\u0438\u043d\u0430?!",
*   "type":"1",
*   "kind":"node"},
*
*  "edges":[
*    {"id":"35",
*    "to":"21",
*    "label":"\u041c\u0430\u043a\u0441\u0438\u043c\u0438\u043b\u043b\u0438\u0430\u043d\u0441",
*    "delay":"15",
*    "kind":"edge"},
*
*    {"id":"36",
*    "to":"22",
*    "label":"\u0412 \u041a\u0438\u0442\u0430\u0439\u0449\u0438\u043d\u0443!",
*    "delay":"20",
*    "kind":"edge"}
*    ]
*  }
* */
