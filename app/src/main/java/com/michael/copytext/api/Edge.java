package com.michael.copytext.api;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by michael on 27.05.16.
 */
public class Edge implements Serializable{

    @SerializedName("id")
    public int id;

    @SerializedName("to")
    public int to;

    @SerializedName("label")
    public String label;

    @SerializedName("delay")
    public int delay;

    @SerializedName("kind")
    public String kind = "edge";

}
