package com.michael.copytext.api;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by michael on 27.05.16.
 */
public class Node implements Serializable {

    @SerializedName("id")
    public int id;

    @SerializedName("label")
    public String label;

    @SerializedName("type")
    public int type;

    @SerializedName("kind")
    public String kind = "node";
}
