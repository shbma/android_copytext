package com.michael.copytext;

import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.michael.copytext.api.*;
import com.michael.copytext.json.UsersDetails;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by michael on 26.05.16.
 */
public class JsonTestActivity extends Activity {
    TextView textRegion;
    GameAPI gameAPI;
    State state;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.json_test);

        initViews();
        initNet();
        askGameState("StartGame",0);

        jsonManipulations();

    }

    /**
     * Инициализируем представления
     */
    public void initViews(){
        textRegion = (TextView) findViewById(R.id.forJsonTextView);
    }

    /**
     * Инициализируем объект для общения с сервером
     */
    public void initNet(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        gameAPI = retrofit.create(GameAPI.class);
    }

    /**
     * Запрашиваем у сервера состояние с данным id
     * @param jax "StartGame" - начало игры, "Step" - след.шаг
     * @param id айдишник из БД
     */
    public void askGameState(String jax, int id){

        Call<State> requestOfState = gameAPI.getState(jax, id);
        requestOfState.enqueue(new Callback<State>() {

            @Override
            public void onResponse(Call<State> call, Response<State> response) {
                if (response.isSuccessful()){
                    state = response.body();
                    textRegion.setText("success" + state.node.label);
                } else {
                    textRegion.setText("filure");
                }
            }

            @Override
            public void onFailure(Call<State> call, Throwable t) {

            }
        });
    }

    //играемся с json-ом
    public void jsonManipulations(){
        UsersDetails user = new UsersDetails();
        Gson gson = new Gson();
        String json = gson.toJson(user);
        textRegion.setText(json);
    }

}
