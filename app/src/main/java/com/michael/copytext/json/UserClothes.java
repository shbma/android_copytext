package com.michael.copytext.json;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by michael on 26.05.16.
 */
public class UserClothes implements Serializable {

    @SerializedName("type")
    public String type =  "tshirt";

    @SerializedName("size")
    public int size = 46;

    @SerializedName("material")
    public String material = "cotton";
}
