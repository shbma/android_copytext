package com.michael.copytext.json;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by michael on 26.05.16.
 */
public class UsersDetails implements Serializable{

    @SerializedName("UserName")
    public String name="Ivanoff Ivan";

    @SerializedName("UserFloor")
    public int floor=5;

    @SerializedName("clothers")
    public  UserClothes clothes = new UserClothes();

    @SerializedName("skills")
    public String[] skills = {"html", "js", "css"};

}
