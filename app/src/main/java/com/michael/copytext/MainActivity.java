package com.michael.copytext;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.DialogPreference;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;

public class MainActivity extends AppCompatActivity implements Serializable {

    private Button buttonPaste;
    private Button buttonRedesign;
    private Button buttonJson;

    private EditText textArea;
    private TextView bufferMirror;

    private ClipboardManager clipboard;
    private ClipData clip;
    String pasteData = "";

    private AlertDialog.Builder sureDialog; //диалог "Вы уверены?"

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initTextArea(); //покажем текст
        initBuffer();

        initButtons();
        setButtonPasteBehaviour();
        updateButtonPasteView();

        setButtonRedesignBehaviour();

        initSureDialog();

        setBufferMirrorBehaviour();
        setButtonJSONBehaviour();

    }

    /**
     * инициализируем и наполняем текстовые области
     */
    public void initTextArea(){
        textArea = (EditText) findViewById(R.id.textField);
        textArea.setText(R.string.testText);

        bufferMirror = (TextView) findViewById(R.id.bufferMirror);
    }

    /**
     * инициализируем буфер обмена
     */
    public void initBuffer(){
        clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
    }

    /**
     * инициализируем кнопки - вставки и редизайна
     */
    public void initButtons(){
        buttonPaste = (Button) findViewById(R.id.buttonPaste);
        buttonRedesign = (Button) findViewById(R.id.buttonRedesign);
        buttonJson = (Button) findViewById(R.id.go_to_JSON_test_button);
    }

    /**
     * обработчики кнопки Перейти к JSON тестовой странице
     */
    private void setButtonJSONBehaviour(){

        buttonJson.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v){
               Intent intent = new Intent(getApplicationContext(), JsonTestActivity.class);
               startActivity(intent);
            }
        });

    }

    /**
     * обработчики кнопки Вставить
     */
    private void setButtonPasteBehaviour(){

        buttonPaste.setOnClickListener(new View.OnClickListener() {
            String tmp_text;

            @Override
            public void onClick(View v){
                ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);

                pasteData = item.getText().toString(); // извлекаем текст

                tmp_text = bufferMirror.getText().toString();
                bufferMirror.setText(tmp_text + pasteData); // вставляем в целевой элемент
            }
        });

    }

    /**
     * установка обработчиков на текстовую область для вставок
     */
    public void setBufferMirrorBehaviour(){
        bufferMirror.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                sureDialog.show();
            }
        });
    }

    /**
     * делает кнопку Вставить активной/неактивной смотря по заполненности буфера
     *
     * http://developer.android.com/intl/ru/guide/topics/text/copy-paste.html
     */
    public void updateButtonPasteView(){
        if (!(clipboard.hasPrimaryClip())) {

            buttonPaste.setClickable(false);
            buttonPaste.setTextColor(getResources().getColor(R.color.buttonPastePassive));

        } else if (clipboard.getPrimaryClip().getItemAt(0).getText().length() <= 0 ) {

            // Пассивируем кнопку - т.к. буфер пуст и вставлять нечего
            buttonPaste.setClickable(false);
            buttonPaste.setTextColor(getResources().getColor(R.color.buttonPastePassive));

        } else {
            // Активизируем кнопку - т.к. в буфере есть текст
            buttonPaste.setClickable(true);
            buttonPaste.setTextColor(getResources().getColor(R.color.buttonPasteActive));
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    /**
     * обработчики главного меню
     * @param item нажатый пункт меню
     * @return ture/false
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id){

            case R.id.itemCopy:
                copyAll();
                break;
            case R.id.itemCopyFirst:
                copyFistLine();
                break;
            case R.id.itemClear:
                clearAll();
                break;
            case R.id.itemClearFirst:
                clearFirstWord();
                break;
        }
        updateButtonPasteView(); //обновим вид кнопки Вставить

        return super.onOptionsItemSelected(item);
    }

    /**
     * копируем весь текст в буфер
     */
    public void copyAll(){
        String textFrom;

        textFrom = textArea.getText().toString(); //добываем текст

        clip = ClipData.newPlainText("textBrick", textFrom);
        clipboard.setPrimaryClip(clip); // put the new clip object on the clipboard
    }

    /**
     * копируем первую строку в буфер -- примерно
     */
    public void copyFistLine(){

        float k = 3; //экспериментальный поправочный коэффицинт

        //нашли длину первой строки (примерно)
        float fontsize = textArea.getTextSize(); //px
        int numberOfChars = Math.round(k * convertPx2Dp(MainActivity.this, textArea.getWidth()) / fontsize);

        //достаем первую строку (примерно) из текста и сохраняем в буфер
        pasteData = textArea.getText().toString().substring(0, numberOfChars);

        clip = ClipData.newPlainText("textBrick", pasteData);
        clipboard.setPrimaryClip(clip);

        Toast.makeText(getApplicationContext(), "Первая строка плюс-минус слово (а может и словА) добавлена в буфер обмена.",
                Toast.LENGTH_LONG).show();
    }

    /**
     * затираем весь текст из буфера
     */
    public void clearAll(){
        clip = ClipData.newPlainText("","");
        clipboard.setPrimaryClip(clip);

        //может есть более изящный способ, именно очистить а не затереть?
    }

    /**
     * стираем первое слово из буфера
     */
    public void clearFirstWord(){

        //извлекаем из буфера текущий текст
        ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);
        pasteData = item.getText().toString(); // извлекаем текст

        //удаляем 1е слово
        pasteData = pasteData.replaceAll("(?=(^|\\n)\\s*?)\\S+ ","");

        //пишем обновленный текст обратно в буфер
        clip = ClipData.newPlainText("textBrick", pasteData);
        clipboard.setPrimaryClip(clip);
    }

    /**
     * задаем обработчики кнопке Редизайна
     */
    public void setButtonRedesignBehaviour(){
        buttonRedesign.setOnClickListener(viewButtClickListener);
    }

    /**
     * скелет обработчика клика
     */
    View.OnClickListener viewButtClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showThemePopupMenu(v);
        }
    };

    /**
     * показать всплывающее меню выбора стиля
     */
    private void showThemePopupMenu(View v) {

        PopupMenu popupMenu = new PopupMenu(this, v);
        popupMenu.inflate(R.menu.popoup_menu_redesign);

        popupMenu.setOnMenuItemClickListener(
                new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int id = item.getItemId();

                        switch (id) {
                            case R.id.theme_1:
                                textArea.setTextColor(getResources().getColor(R.color.tarChina));
                                return true;
                            case R.id.theme_2:
                                textArea.setTextColor(getResources().getColor(R.color.tarNiderlandy));
                                return true;
                            case R.id.theme_sub_1:
                                textArea.setTextColor(getResources().getColor(R.color.tarRussiaSummer));
                                return true;
                            case R.id.theme_sub_2:
                                textArea.setTextColor(getResources().getColor(R.color.tarRussiaWinter));
                                return true;
                            default:
                                return false;
                        }
                    }
                });

        popupMenu.show();
    }

    /**
     * формируем диалог "Вы уверены?" с двумя кнопками
     */
    public void initSureDialog() {
        sureDialog = new AlertDialog.Builder(MainActivity.this);

        sureDialog.setTitle(getResources().getText(R.string.dialTitle));  // заголовок
        sureDialog.setMessage(getResources().getText(R.string.dialMessage)); // сообщение
        sureDialog.setPositiveButton(getResources().getText(R.string.dialTitleYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                Toast.makeText(getApplicationContext(), "Ок, ломать не строить",
                        Toast.LENGTH_LONG).show();
                bufferMirror.setText("");
            }
        });
        sureDialog.setNegativeButton(getResources().getText(R.string.dialTitleNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                Toast.makeText(getApplicationContext(), "Зачем тогда вызывал?", Toast.LENGTH_LONG)
                        .show();
            }
        });
        sureDialog.setCancelable(true);
    }

    /**
     * Преобразователь  px -> dp
     *
     * @param context Context
     * @param pxValue px unit
     * @return The converted size in pixel
     *
     * https://github.com/Jason-Lee-1001/JFrame/blob/master/jframework/src/main/java/com/studio/jframework/utils/SizeUtils.java
     */
    public static int convertPx2Dp(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

//    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//        outState.putSerializable("activity_key", this);
//    }
//
//    @Override
//    protected void onRestoreInstanceState(Bundle savedInstanceState) {
//        super.onRestoreInstanceState(savedInstanceState);
//        MainActivity activity = (MainActivity)savedInstanceState.getSerializable("activity_key");
//    }
}
